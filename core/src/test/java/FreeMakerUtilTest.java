import ldh.maker.util.FreeMakerUtil;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ldh123 on 2018/7/18.
 */
public class FreeMakerUtilTest {

    @Test
    public void testName() {
        String tableName = "t_demo_ad_";
        String name = FreeMakerUtil.javaName(tableName);
        Assert.assertEquals("tDemoAd", name);
    }

    @Test
    public void replace() {
        String pp = "ldh.demo.xml";
        String t = pp.replace(".", "/");
        System.out.println("t:" + t);

        pp = "ldhxml";
        t = pp.replace(".", "/");
        System.out.println("t:" + t);
    }
}
