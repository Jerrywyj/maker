package ldh.maker.freemaker;

import ldh.database.Table;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ldh on 2017/4/16.
 */
public class JavafxEditControllerMaker extends BeanMaker<JavafxEditControllerMaker> {

    protected Table table;
    protected String projectPackage;
    protected String pojoPackage;
    protected String enumProjectPackage;

    public JavafxEditControllerMaker table(Table table) {
        this.table = table;
        return this;
    }

    public JavafxEditControllerMaker projectPackage(String projectPackage) {
        this.projectPackage = projectPackage;
        return this;
    }

    public JavafxEditControllerMaker pojoPackage(String pojoPackage) {
        this.pojoPackage = pojoPackage;
        return this;
    }

    public JavafxEditControllerMaker enumProjectPackage(String enumProjectPackage) {
        this.enumProjectPackage = enumProjectPackage;
        return this;
    }

    @Override
    public JavafxEditControllerMaker make() {
        data();
        out(ftl, data);
        return this;
    }

    @Override
    public void data() {
        fileName = table.getJavaName() + "EditController.java";
        data.put("table", table);
        data.put("projectPackage", projectPackage);
        data.put("pojoPackage", pojoPackage);
        data.put("controllerPackage", pack);
        data.put("enumProjectPackage", enumProjectPackage);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str=sdf.format(new Date());
        data.put("Author", author);
        data.put("DATE", str);
        data.put("Description",description);
    }
}
