package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Column;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.*;
import ldh.maker.util.CopyDirUtil;
import ldh.maker.util.EnumFactory;
import ldh.maker.util.FileUtil;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;


/**
 * Created by ldh on 2017/4/6.
 */
public abstract class JavafxCreateCode extends CreateCode{

    protected String javafxPath = "";

    public JavafxCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
    }

    @Override
    protected void buildSwaggerMaker() {

    }

    protected void copyResource() {
        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, getProjectName(), "src", "main", "resources"));
        try {
            copyResources("css", dirs, "css");
            copyResources("images", dirs, "images");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void saveJson(String fileName, String json, String... packs) throws IOException {
        String jsonPath = createResourcePath(packs);
        String file = jsonPath + "/" + fileName;
        FileUtil.saveFile(file, json);
    }

    protected void buildUtilFile(TableInfo tableInfo, String packageDir, String ftl, String javaFileName) {
        String fxml = createResourcePath("resources", "fxml");
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + "." + packageDir);
        new JavafxMaker()
                .projectPackage(projectPackage)
                .author(data.getAuthorProperty())
                .tableInfo(tableInfo)
                .ftl(ftl)
                .fileName(javaFileName)
                .outPath(path)
                .make();
    }

    protected JavafxPojoMaker buildJavafxPojoMaker(Table table, String pojoPath, Class<?> key, KeyMaker keyMaker) {
        return (JavafxPojoMaker) new JavafxPojoMaker()
                .pack(javafxPath)
                .table(table)
                .key(key, keyMaker)
                .outPath(pojoPath);
    }

    protected void buildJavafxEditFormMaker(Table table, String ftl) {
        String path = createResourcePath("resources", "fxml", "module", FreeMakerUtil.firstLower(table.getJavaName()));
        String projectPackage = this.getProjectRootPackage(javafxPath);
        new JavafxEditFormMaker()
                .table(table)
                .projectPackage(projectPackage)
                .pack(data.getControllerPackageProperty())
                .ftl(ftl)
                .outPath(path)
                .make();
    }

    protected void buildJavafxMainFormMaker(Table table, String ftl) {
        String fxml = createResourcePath("resources", "fxml", "module", FreeMakerUtil.firstLower(table.getJavaName()));
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String fileName = table.getJavaName() + "Main.fxml";
        new JavafxMaker()
                .projectPackage(projectPackage)
                .pack(data.getControllerPackageProperty())
                .table(table)
                .ftl(ftl)
                .fileName(fileName)
                .outPath(fxml)
                .make();
    }

    protected void buildJavafxMainControllerMaker(Table table, String ftl) {
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + ".controller." + FreeMakerUtil.firstLower(table.getJavaName()));
        new JavafxMainControllerMaker()
                .table(table)
                .projectPackage(projectPackage)
                .author(data.getAuthorProperty())
                .description(table.getComment())
                .enumProjectPackage(getEnumProjectPackage())
                .pack(data.getControllerPackageProperty())
                .pojoPackage(javafxPath)
                .servicePackage(data.getServicePackageProperty())
                .ftl(ftl)
                .outPath(path)
                .make();
    }

    protected void buildJavafxEditControllerMaker(Table table, String ftl) {
        String projectPackage = this.getProjectRootPackage(javafxPath);
        String path = this.createPath(projectPackage + ".controller." + FreeMakerUtil.firstLower(table.getJavaName()));
        String enumPackage = "";
        for (Column column : table.getColumnList()) {
            if (FreeMakerUtil.isEnum(column)) {
                String keyId = treeItem.getParent().getValue().getId() + "_" + dbName + "_" + table.getName() + "_" + column.getName();
                EnumStatusMaker enumStatusMaker = EnumFactory.getInstance().get(keyId);
                enumPackage = enumStatusMaker.getPack();
//                imports.add(enumStatusMaker.getPack() + "." + column.getJavaType());
//                imports.add(ValuedEnumObjectSerializer.class.getName());
            }
        }
        new JavafxEditControllerMaker()
                .table(table)
                .projectPackage(projectPackage)
                .author(data.getAuthorProperty())
                .description(table.getComment())
                .enumProjectPackage(enumPackage)
                .pack(data.getControllerPackageProperty())
                .pojoPackage(javafxPath)
                .ftl(ftl)
                .outPath(path)
                .make();
    }

    protected void buildJavafxSearchFormMaker(Table table, String ftl) {
        String path = createResourcePath("resources", "fxml", "module", FreeMakerUtil.firstLower(table.getJavaName()));
        String projectPackage = this.getProjectRootPackage(javafxPath);
        new JavafxSearchFormMaker()
                .table(table)
                .projectPackage(projectPackage)
                .pack(data.getControllerPackageProperty())
                .ftl(ftl)
                .outPath(path)
                .make();
    }

    protected void buildPomXmlMaker() {
        String projectRootPackage = getProjectRootPackage(javafxPath);
        String resourcePath = createPomPath();
        new PomXmlMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(this.getProjectName())
                .outPath(resourcePath)
                .ftl("client/pom.ftl")
                .make();
    }

    protected void copyResources(String srcDir, List<String> dirs) throws IOException {
        String root = FileUtil.getSourceRoot();
        Enumeration<URL> urls = Thread.currentThread().getContextClassLoader().getResources(srcDir);
        String srcFile = urls.nextElement().getFile();
        CopyDirUtil.copyResourceDir(srcFile, root, dirs);
    }

    protected String getEnumProjectPackage() {
        String enumPackage = "";
        for (Column column : table.getColumnList()) {
            if (FreeMakerUtil.isEnum(column)) {
                String keyId = treeItem.getParent().getValue().getId() + "_" + dbName + "_" + table.getName() + "_" + column.getName();
                EnumStatusMaker enumStatusMaker = EnumFactory.getInstance().get(keyId);
                enumPackage = enumStatusMaker.getPack();
            }
        }
        return enumPackage;
    }
}
