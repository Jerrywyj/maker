package ${projectPackage}.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Popup;
import javafx.util.Duration;
import ldh.fx.component.LNavPane;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class HomeController implements Initializable {

    @FXML private HomeHeadController headController;
    @FXML private HomeLeftController leftController;
    @FXML private VBox contentPane;
    @FXML private LNavPane leftPane;

    private Map<Button, Popup> popupMap = new HashMap<>();

    public void tongleLeftPane() {
        leftPane.tongleLeftPane();
    }

    @FXML public void navTitleBtn(ActionEvent e) {
        e.consume();
    }

    @FXML public void showDashboard() {
        System.out.println("show dashboard!!!!!!!!");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        headController.setHomeController(this);
        leftController.setHomeController(this);
        leftPane.setPrefWidth(220);
        leftPane.setMinWidth(220);
    }

    public Pane getContentPane() {
        return contentPane;
    }
}
