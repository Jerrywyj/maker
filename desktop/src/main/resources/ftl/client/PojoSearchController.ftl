package ${projectPackage}.controller.${util.firstLower(table.javaName)};

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import ldh.fx.component.table.function.ValuedEnumStringConverter;

import ${pojoPackage}.${table.javaName};
<#list table.columnList as column>
    <#if util.isEnum(column)>
import ${enumProjectPackage}.${column.javaType};
    </#if>
</#list>

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ${table.javaName}SearchController implements Initializable{

    ${table.javaName}MainController ${util.firstLower(table.javaName)}MainController;

    <#list table.columnList as column>
        <#if util.isDate(column)>
    @FXML private DatePicker ${column.property};
        <#elseif util.isEnum(column)>
    @FXML private ChoiceBox ${column.property}ChoiceBox;
        <#else>
    @FXML private TextField ${column.property}Text;
        </#if>
    </#list>

    public void set${table.javaName}MainController(${table.javaName}MainController ${util.firstLower(table.javaName)}MainController) {
        this.${util.firstLower(table.javaName)}MainController = ${util.firstLower(table.javaName)}MainController;
    }

    @FXML private void searchAct() {
        ${util.firstLower(table.javaName)}MainController.setSearchParam(buildParams());
        ${util.firstLower(table.javaName)}MainController.load(null);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        <#list table.columnList as column>
            <#if util.isEnum(column)>
        ${column.property}ChoiceBox.getItems().addAll(${column.javaType}.values());
        ${column.property}ChoiceBox.setConverter(new ValuedEnumStringConverter());
            </#if>
        </#list>
    }

    public Map<String, Object> buildParams(){
        Map<String, Object> paramMap = new HashMap<String, Object>();
        <#list table.columnList as column>
            <#if util.isDate(column)>

            <#elseif util.isEnum(column)>
        ${column.javaType} ${column.property} = (${column.javaType}) ${column.property}ChoiceBox.getSelectionModel().getSelectedItem();
        if (${column.property} != null) {
            paramMap.put("${column.property}", ${column.property}.getDesc());
        }
            <#elseif column.foreign>
        paramMap.put("${column.property}.${column.foreignKey.foreignTable.primaryKey.columns[0].property}", ${column.property}Text.getText());
            <#else>
        paramMap.put("${column.property}", ${column.property}Text.getText());
            </#if>
        </#list>
        return paramMap;
    }
}
