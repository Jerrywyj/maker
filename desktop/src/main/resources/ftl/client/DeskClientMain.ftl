package ${projectPackage};

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ldh.fx.StageUtil;

public class DeskClientMain extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        StageUtil.STAGE = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Home.fxml"));

        Scene scene = new Scene(root, 1200, 600);
        scene.getStylesheets().add("/css/common.css");
        scene.getStylesheets().add("/component/LNavPane.css");
        scene.getStylesheets().add("/component/LxDialog.css");
        scene.getStylesheets().add("/component/GridTable.css");
        scene.getStylesheets().add("/css/Form.css");

        primaryStage.setTitle("代码自动生成框架");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}