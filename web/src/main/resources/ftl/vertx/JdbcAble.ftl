package ${beanPackage};

/**
 * @author: ${Author}
 * @date: ${DATE}
 */

import co.paralleluniverse.fibers.Instrumented;
import co.paralleluniverse.fibers.Suspendable;

@FunctionalInterface
public interface JdbcAble<T, R> {

    @Instrumented
    @Suspendable
    R apply(T t);
}
